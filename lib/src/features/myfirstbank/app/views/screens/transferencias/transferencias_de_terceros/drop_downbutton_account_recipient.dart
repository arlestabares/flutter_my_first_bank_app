import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../../data/models/model_list/cuentas_propias.dart';

// ignore: must_be_immutable
class DropDownButtonAccountRecipient extends StatefulWidget {
  final String? dropDownButtonSVGImage;
  Function(String iban, String account)? dataChangedClosure;

  DropDownButtonAccountRecipient({
    @required this.dropDownButtonSVGImage,
    @required this.dataChangedClosure,
  });

  @override
  _DropDownButtonAccountRecipientState createState() =>
      _DropDownButtonAccountRecipientState();
}

class _DropDownButtonAccountRecipientState
    extends State<DropDownButtonAccountRecipient> {
  String? opcionSelecconada = 'Cuentas propias';
  String iban = '';
  String account = '';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10),
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          height: 60,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(color: Color(0xFFE5E5E5)),
          ),
          child: Row(
            children: [
              Expanded(
                child: DropdownButton(
                  icon: SvgPicture.asset(widget.dropDownButtonSVGImage!),
                  isExpanded: true,
                  value: opcionSelecconada!,
                  //items: miLista
                  items: ownAccounts
                      .map<DropdownMenuItem<String>>(
                        (lista) => DropdownMenuItem<String>(
                          value: lista.name,
                          child: Text(lista.name),
                        ),
                      )
                      .toList(),
                  onChanged: (value) {
                    var selected = ownAccounts
                        .firstWhere((element) => element.name == value);
                    opcionSelecconada = value as String;

                    widget.dataChangedClosure!(
                        selected.iban, selected.currentOrSavings);
                    setState(() {
                      iban = selected.iban;
                      account = selected.currentOrSavings;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'IBAN: $iban',
              style: TextStyle(fontSize: 13),
            ),
            Text('Cuenta: $account', style: TextStyle(fontSize: 13)),
          ],
        ),

        SizedBox(height: 40.0),

      ],
    );
  }
}
