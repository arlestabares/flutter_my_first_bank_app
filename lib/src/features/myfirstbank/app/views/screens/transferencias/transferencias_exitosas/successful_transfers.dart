import 'package:flutter/material.dart';

import '../../../../utils/constants.dart';

class TransferenciaExitosa extends StatelessWidget {
  final String? alias;
  final String? name;
  final String? iban;
  final String? currentOrSavings;
  final String? ibanOrigin;
  final String? currentOrSavingsOrigin;

  const TransferenciaExitosa({
    this.alias,
    this.name,
    this.iban,
    this.currentOrSavings,
    this.ibanOrigin,
    this.currentOrSavingsOrigin,
  });
  // alias: widget.aliasDestino,
  // name: widget.nameDestino,
  // iban: widget.ibanDestino,
  // currentOrSavings: widget.currentOrSavingsDestino,
  // ibanOrigin: widget.ibanOrigin,
  // currentOrSavingsOrigin:
  //     widget.currentOrSavingsOrigin,

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tranferencia exitosa'),
        automaticallyImplyLeading: false,
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            TransfersDoneList(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.arrow_back),
        onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
            'myFirstBankHomePage', (Route<dynamic> route) => false),
      ),
    );

    //MyFirstBankBodyScreen())),
  }
}

class TransfersDoneList extends StatelessWidget {
  final String? alias;
  final String? name;
  final String? iban;
  final String? currentOrSavings;
  //
  final String? ibanOrigin;
  final String? currentOrSavingsOrigin;

  TransfersDoneList({
    this.alias,
    this.name,
    this.iban,
    this.currentOrSavings,
    this.ibanOrigin,
    this.currentOrSavingsOrigin,
  });

  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
    return Column(
      children: [
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        ),
        TransfersOptionCard2(
          alias: 'TA',
          name: 'Arles Tabares',
          iban: 'ES5601823107',
          currentOrSavings: 'Cuenta\n\nCorriente',
          ibanOrigin: ibanOrigin,
          currentOrSavingsOrigin: currentOrSavingsOrigin,
        )
      ],
    );
  }
}

//clase
class TransfersOptionCard2 extends StatelessWidget {
  // final VoidCallback? pressed;

  final String? alias;
  final String? name;
  final String? iban;
  final String? currentOrSavings;
  final String? ibanOrigin;
  final String? currentOrSavingsOrigin;

  TransfersOptionCard2({
    // @required this.pressed,
    @required this.alias,
    @required this.name,
    @required this.iban,
    @required this.currentOrSavings,
    @required this.ibanOrigin,
    @required this.currentOrSavingsOrigin,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      width: size.width,
      height: 80.0,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 25),
            blurRadius: 30.0,
            spreadRadius: 10,
            color: Colors.white70,
          ),
        ],
      ),
      child: Card(
        elevation: 8.0,
        shadowColor: kShadowColor,
        child: Center(
          child: ListTile(
            leading: CircleAvatar(child: Text(alias!)),
            title: Text(name!),
            subtitle: Text(iban!),
            trailing: Text(currentOrSavings!),
          ),
        ),
      ),
    );
  }
}
