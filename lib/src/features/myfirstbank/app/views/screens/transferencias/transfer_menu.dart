import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_exitosas/successful_transfers.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_propias/widgets/transference_background_header.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_propias/widgets/transference_header.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/widgets/transfers_options_list_view.dart';

import '../../../utils/constants.dart';

class TransfersInformationScreen extends StatefulWidget {
  @override
  _TransfersInformationScreenState createState() =>
      _TransfersInformationScreenState();
}

class _TransfersInformationScreenState
    extends State<TransfersInformationScreen> {
  TextEditingController iBantextEditingController = TextEditingController();
  TextEditingController nameTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
  final size = MediaQuery.of(context).size;
    final int? accountNumber = 1234;
    //final double amountAvailable = 875.27;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text('Transferencias/Traspasos'),
        backgroundColor: kBackgroundColor,
      ),
      body: Stack(
        children: [
          TranfersBackgroundHeader(),
          TransfersHeader(
            accountNumber: accountNumber,
            amountAvailable: 73645,
          ),
          Padding(
            padding: EdgeInsets.only(
              // vertical: 190.0,
              top: (size.width <= 400.0)? 140.0: 190.0,
            ),
            child: SingleChildScrollView(
              child: Container(
              width: size.width,
                child: Column(
                  children: [
                    TransfersOptionsHorizontalListView(),
                    TransfersDoneList(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
