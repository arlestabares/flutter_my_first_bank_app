import 'package:flutter/material.dart';

import '../../../../../utils/constants.dart';
import '../../transferencias_de_terceros/drop_downbutton_account_recipient.dart';
import '../../transferencias_de_terceros/drop_downbutton_origen_account.dart';
import '../../transferencias_de_terceros/screens/select_amount_to_transfers_screen.dart';
import '../widgets/transference_background_header.dart';
import '../widgets/transference_header.dart';

class SelectDataForTransfersOwnScreen extends StatefulWidget {
  @override
  _SelectDataForTransfersOwnScreenState createState() =>
      _SelectDataForTransfersOwnScreenState();
}

class _SelectDataForTransfersOwnScreenState
    extends State<SelectDataForTransfersOwnScreen> {
  TextEditingController iBantextEditingController = TextEditingController();
  TextEditingController nameTextEditingController = TextEditingController();
  String aliasOrigin = '';
  String nameOrigin = '';
  String ibanOrigin = '';
  String accountOrigin = '';

  String aliasDestinatario = '';
  String nameDestinatario = '';
  String ibanDestinatario = '';
  String accountDestinatario = '';

  @override
  Widget build(BuildContext context) {
    final int? accountNumber = 1234;
    //final double amountAvailable = 875.27;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text('Transferencias/Traspasos'),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                TranfersBackgroundHeader(),
                TransfersHeader(
                  accountNumber: accountNumber,
                  amountAvailable: 73645,
                ),
              ],
            ),
            SizedBox(height: 20.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Seleccionar cuenta destino',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10.0),
            Container(
              width: double.infinity,
              child: DropDownButtonAccountRecipient(
                dropDownButtonSVGImage: 'assets/icons/dropdown.svg',
                dataChangedClosure: (iban, account) {
                  this.ibanDestinatario = iban;
                  this.accountDestinatario = account;
                },
              ),
            ),
            SizedBox(height: 10.0),
            Text(
              'Seleccionar cuenta origen',
              style: TextStyle(
                color: Colors.black,
                fontSize: 17.0,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 10.0),
            Container(
              width: double.infinity,
              child: DropDownButtonOrigenAccount(
                dropDownButtonSVGImage: 'assets/icons/dropdown.svg',
                dataChangedClosure: (alias, name, iban, account) {
                  this.aliasOrigin = alias;
                  this.nameOrigin = name;
                  this.ibanOrigin = iban;
                  this.accountOrigin = account;
                },
              ),
            ),
            SizedBox(height: 10.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 45.0),
              child: ElevatedButton(
                onPressed: () {
                  print('Informacion del IBAN:::$ibanOrigin');
                  print('Informacion del IBAN:::$ibanDestinatario');
                  if (ibanDestinatario.isEmpty || ibanDestinatario == 'Cuentas propias') {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Debes elegir la cuenta destino'),
                      ),
                    );
                    return;
                  } else if (ibanOrigin.isEmpty|| ibanOrigin == 'Cuentas propias') {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Debes elegir la cuenta origen'),
                      ),
                    );
                    return ;
                  }
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => SelectAmountToTransfersScreen(
                        aliasDestino: aliasDestinatario,
                        nameDestino: nameDestinatario,
                        ibanDestino: ibanOrigin,
                        currentOrSavingsDestino: accountOrigin,
                        //
                        ibanOrigin: ibanDestinatario,
                        currentOrSavingsOrigin: accountDestinatario,
                      ),
                    ),
                  );
                },
                child: Text(
                  'Continuar',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(
                    horizontal: 45.0,
                    vertical: 15.0,
                  ),
                  primary: kBackgroundColor,
                  onPrimary: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immu
