import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_exitosas/successful_transfers.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_propias/widgets/transference_background_header.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_propias/widgets/transference_header.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/model_list/amount_account.dart';

import '../../../../../utils/constants.dart';

class SelectAmountToTransfersScreen extends StatefulWidget {
  final String? aliasDestino;
  final String? nameDestino;
  final String? ibanDestino;
  final String? currentOrSavingsDestino;
  //
  final String? ibanOrigin;
  final String? currentOrSavingsOrigin;

  const SelectAmountToTransfersScreen({
    @required this.aliasDestino,
    @required this.nameDestino,
    @required this.ibanDestino,
    @required this.currentOrSavingsDestino,
    //
    @required this.ibanOrigin,
    @required this.currentOrSavingsOrigin,
  });

  @override
  _SelectAmountToTransfersScreenState createState() =>
      _SelectAmountToTransfersScreenState();
}

class _SelectAmountToTransfersScreenState
    extends State<SelectAmountToTransfersScreen> {
  TextEditingController amountEditingController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  String amount = '';

  //
  @override
  void dispose() {
    amountEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final int? accountNumber = 1234;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text('Transferencias/Traspasos'),
        backgroundColor: kBackgroundColor,
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  TranfersBackgroundHeader(),
                  TransfersHeader(
                    accountNumber: accountNumber,
                    amountAvailable: 73645,
                  ),
                ],
              ),
              SizedBox(height: 80.0),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Ingresar Monto a transferir',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20.0),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 21.0),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    amount = value;
                  },
                  controller: amountEditingController,
                  validator: (value) {
                    //
                    if (value != null && value.isNotEmpty) {
                      var stringToInt = int.parse(value);
                      if (value.isEmpty) {
                        return 'Por favor ingrese un valor';
                      } else if (stringToInt > saldoA || stringToInt == 0) {
                        return 'Saldo insuficiente...verifique disponible';
                      }
                    }
                  },
                ),
              ),
              SizedBox(height: 80.0),
              Container(
                margin: EdgeInsets.only(top: 50.0),
                padding: EdgeInsets.symmetric(horizontal: 45.0),
                child: ElevatedButton(
                  onPressed: () {
                    print(amount);
                    if (amount.isNotEmpty) {
                      var amount2 = int.parse(amount);
                      if (amount2 > saldoA  ) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text('Debes ingresar un monto de dinero menor'),
                          ),
                        );
                        return;
                      }
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text('Processing Data'),
                        ),
                      );
                      return;
                    }

                    print('Data${widget.aliasDestino}');
                    print('Data${widget.nameDestino}');
                    print('Data${widget.ibanDestino}');
                    print('Data${widget.currentOrSavingsDestino}');

                    print('Informacion del IBAN:::${widget.ibanOrigin}');
                    print(
                        'Informacion del IBAN:::${widget.currentOrSavingsOrigin}');

                    print('.........${amountEditingController.text}');
                  

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => TransferenciaExitosa(
                          alias: widget.aliasDestino,
                          name: widget.nameDestino,
                          iban: widget.ibanDestino,
                          currentOrSavings: widget.currentOrSavingsDestino,
                          //
                          ibanOrigin: widget.ibanOrigin,
                          currentOrSavingsOrigin: widget.currentOrSavingsOrigin,
                        ),
                      ),
                    );
                  },
                  child: Text(
                    'Continuar',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(
                      horizontal: 45.0,
                      vertical: 15.0,
                    ),
                    primary: kBackgroundColor,
                    onPrimary: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
