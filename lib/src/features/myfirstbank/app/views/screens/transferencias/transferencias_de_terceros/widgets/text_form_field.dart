import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/utils/constants.dart';

class TextFormFieldSelectAmount extends StatefulWidget {
  @override
  _TextFormFieldSelectAmountState createState() =>
      _TextFormFieldSelectAmountState();
}

class _TextFormFieldSelectAmountState extends State<TextFormFieldSelectAmount> {
  final _forKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      child: Column(
        children: [
          TextFormField(
            validator: (value) {
              if (value!.isEmpty) {
                return 'Debe Ingresar una cantidad';
              }
            },
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 16.0),
            child: ElevatedButton(
              onPressed: () {
                if (_forKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('Procesando datos')));
                }
              },
              child: Text(
                'Continuar',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                ),
              ),
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.symmetric(
                  horizontal: 45.0,
                  vertical: 15.0,
                ),
                primary: kBackgroundColor,
                onPrimary: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
