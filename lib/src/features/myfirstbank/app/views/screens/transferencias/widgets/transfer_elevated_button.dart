import 'package:flutter/material.dart';

import '../../../../utils/constants.dart';
class TransferElevatedButton extends StatelessWidget {
  final String? label;
  final VoidCallback? pressed;
  const TransferElevatedButton({
    @required this.label,
    @required this.pressed,
  });
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: pressed,
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 17.0),
        primary: kBackgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        //onSurface: kBackgroundColor,
      ),
      child: Text(
        label!,
        style: TextStyle(
          color: Colors.white.withOpacity(0.8),
          fontSize: 17,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}