import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/utils/constants.dart';

import '../widgets/transference_background_header.dart';
import '../widgets/transference_header.dart';

class OwnTransfersScreen extends StatefulWidget {
  @override
  _OwnTransfersScreenState createState() => _OwnTransfersScreenState();
}

class _OwnTransfersScreenState extends State<OwnTransfersScreen> {
  TextEditingController iBantextEditingController = TextEditingController();
  TextEditingController nameTextEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final int? accountNumber = 1234;
    //final double amountAvailable = 875.27;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text('Transferencias/Traspasos'),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                TranfersBackgroundHeader(),
                TransfersHeader(
                  accountNumber: accountNumber,
                  amountAvailable: 73645,
                ),
              ],
            ),
            SizedBox(height: 80.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Transferencia a una de mis cuentas',
                    style: TextStyle(color: Colors.brown, fontSize: 18),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: Column(
                children: [
                  TransferenceTextInput(
                    labelText: 'IBAN',
                  ),
                  SizedBox(height: 40.0),
                  TransferenceTextInput(
                    labelText: 'Nombre del beneficiario',
                  ),
                ],
              ),
            ),
            SizedBox(height: 40.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 45.0),
              child: ElevatedButton(
                onPressed: () {},
                child: Text(
                  'Continuar',
                  //style: TextStyle(color: Colors.brown),
                ),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(
                    horizontal: 45.0,
                    vertical: 15.0,
                  ),
                  primary: kBackgroundColor,
                  onPrimary: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class TransferenceTextInput extends StatelessWidget {
  final String? labelText;

  const TransferenceTextInput({
    @required this.labelText,
  });
  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: labelText,
      ),
    );
  }
}
