import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/utils/constants.dart';

class TransfersOptionCard extends StatelessWidget {
  final String? titleTransferencia;
  final VoidCallback? pressed;

  const TransfersOptionCard({
    @required this.titleTransferencia,
    @required this.pressed,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.7,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 25),
            blurRadius: 30.0,
            spreadRadius: 10,
            color: Colors.white70,
          ),
        ],
      ),
      child: Card(
        elevation: 8.0,
        shadowColor: kShadowColor,
        child: TextButton(
          onPressed: pressed,
          child: Text(
            titleTransferencia!,
            style: TextStyle(color: kBackgroundColor, fontSize: 17),
          ),
        ),
      ),
    );
  }
}
