import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_de_terceros/screens/select_amount_to_transfers_screen.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_propias/widgets/transference_background_header.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transferencias_propias/widgets/transference_header.dart';

import '../../../../../utils/constants.dart';
import '../drop_downbutton_account_recipient.dart';
import '../drop_downbutton_origen_account.dart';

class SelectDataForTransfersScreen extends StatefulWidget {
  @override
  _SelectDataForTransfersScreenState createState() =>
      _SelectDataForTransfersScreenState();
}

class _SelectDataForTransfersScreenState
    extends State<SelectDataForTransfersScreen> {
  TextEditingController iBantextEditingController = TextEditingController();
  TextEditingController nameTextEditingController = TextEditingController();
  String ibanOrigin = '';
  String accountOrigin = '';
  //
  String aliasDestinatario = '';
  String nameDestinatario = '';
  String ibanDestinatario = '';
  String currentOrSavingsDestino = '';

  @override
  Widget build(BuildContext context) {
    final int? accountNumber = 1234;
    //final double amountAvailable = 875.27;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text('Transferencias/Traspasos'),
        backgroundColor: kBackgroundColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                TranfersBackgroundHeader(),
                TransfersHeader(
                  accountNumber: accountNumber,
                  amountAvailable: 73645,
                ),
              ],
            ),
            SizedBox(height: 80.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Nuevo destinatario',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20.0),
            Container(
              width: double.infinity,
              child: DropDownButtonAccountRecipient(
                dropDownButtonSVGImage: 'assets/icons/dropdown.svg',
                dataChangedClosure: (iban, account) {
                  this.ibanDestinatario = iban;
                  this.currentOrSavingsDestino = account;
                },
              ),
            ),
            SizedBox(height: 40.0),
            Text(
              'Seleccionar cuenta de origen',
              style: TextStyle(
                color: Colors.black,
                fontSize: 17.0,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: 20.0),
            Container(
              width: double.infinity,
              child: DropDownButtonOrigenAccount(
                dropDownButtonSVGImage: 'assets/icons/dropdown.svg',
                dataChangedClosure: (alias, name, iban, account) {
                  this.ibanOrigin = iban;
                  this.accountOrigin = account;
                },
              ),
            ),
            SizedBox(height: 40.0),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 45.0),
              child: ElevatedButton(
                onPressed: () {
                  print('Informacion del IBAN:::$ibanOrigin');
                  print('Informacion del IBAN:::$ibanDestinatario');

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => SelectAmountToTransfersScreen(
                        ibanOrigin: ibanDestinatario,
                        currentOrSavingsOrigin: currentOrSavingsDestino,
                        aliasDestino: aliasDestinatario,
                        ibanDestino: aliasDestinatario,
                        nameDestino: nameDestinatario,
                        currentOrSavingsDestino: currentOrSavingsDestino,
                      ),
                    ),
                  );
                },
                child: Text(
                  'Continuar',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.symmetric(
                    horizontal: 45.0,
                    vertical: 15.0,
                  ),
                  primary: kBackgroundColor,
                  onPrimary: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

// ignore: must_be_immu
