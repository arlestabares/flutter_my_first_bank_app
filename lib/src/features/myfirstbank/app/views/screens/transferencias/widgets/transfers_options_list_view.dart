import 'package:flutter/material.dart';

import '../transferencias_de_terceros/screens/select_data_for_transfers_screen.dart';
import '../transferencias_propias/screens/select_data_for_transfers_own_screen.dart';
import 'transfer_option_card.dart';

class TransfersOptionsHorizontalListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: 110.0,
      // padding: EdgeInsets.symmetric(vertical: 10.0),
      margin: EdgeInsets.symmetric(horizontal: 17.0),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          TransfersOptionCard(
            pressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => SelectDataForTransfersOwnScreen())),
            titleTransferencia: 'Transferencia \ncuentas propias',
          ),
          TransfersOptionCard(
            pressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => SelectDataForTransfersScreen())),
            titleTransferencia: 'Transferencia \notras cuentas',
          ),
          TransfersOptionCard(
            pressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => SelectDataForTransfersScreen())),
            titleTransferencia: 'Otras operaciones \ncuentas propias',
          ),
          TransfersOptionCard(
            pressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => SelectDataForTransfersScreen())),
            titleTransferencia: 'Otras operaciones \notras cuentas',
          ),
        ],
      ),
    );
  }
}
