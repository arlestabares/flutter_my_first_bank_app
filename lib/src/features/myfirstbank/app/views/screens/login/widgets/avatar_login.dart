import 'package:flutter/material.dart';

import '../../../../utils/constants.dart';

class AvatarLogin extends StatelessWidget {
  final String? avatarTitle;

  AvatarLogin({@required this.avatarTitle});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: CircleAvatar(
        backgroundColor: kBackgroundColorCircularAvatar,
        child: Text(avatarTitle!,
            style: TextStyle(
              fontSize: 28,
            )),
        maxRadius: 30.0,
      ),
    );
  }
}
