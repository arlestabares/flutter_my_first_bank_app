import 'package:flutter/material.dart';

class LoginTextField extends StatelessWidget {
  final Icon? icon;
  final String? label;
  final bool? obscureText;
  final String? usuario;
  final TextEditingController? controller;

  const LoginTextField({
    @required this.label,
    @required this.obscureText,
    @required this.controller,
    this.icon,
    this.usuario,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.number,
        obscureText: obscureText!,
        style: TextStyle(color: Colors.white),
        decoration: InputDecoration(
          labelText: label,
          labelStyle: TextStyle(color: Colors.white),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: icon,
        ),
      ),
    );
  }
}
