import 'package:flutter/material.dart';

class AppBarLogin extends StatelessWidget {
  final String? title;

  AppBarLogin({
    @required this.title,
  });
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SafeArea(
      child: Center(
        child: Container(
          margin: EdgeInsets.only(top: (size.width < 450) ? 20.0 : 0),
          child: Text(
            title!,
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
      ),
    );
  }
}
