import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/utils/estilos_text_style.dart';

class LoginTextName extends StatelessWidget {
  final String? name;

  LoginTextName({
    @required this.name,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 50.0),
      child: Center(
        child: Column(
          children: [
            Text('Hola, $name', style: styleTextMessageLogin),
            SizedBox(height: 10.0),
            Text(
              '¿No eres $name?',
              style: styleTextNameLogin,
            )
          ],
        ),
      ),
    );
  }
}
