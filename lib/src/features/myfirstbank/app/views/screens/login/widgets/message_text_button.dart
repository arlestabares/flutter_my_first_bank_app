import 'package:flutter/material.dart';

class MessageTextButton extends StatelessWidget {
  final String? message;

  const MessageTextButton({@required this.message});
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(
        message!,
        style: TextStyle(fontSize: 18.0, color: Colors.white.withOpacity(0.4)),
      ),
      onPressed: () {},
    );
  }
}