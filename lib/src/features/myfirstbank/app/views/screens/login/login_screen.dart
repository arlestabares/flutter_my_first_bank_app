import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/pages/home_page.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/model_list/claves.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/domain/usecases/login_input_decoration.dart';

import '../../../utils/constants.dart';
import 'widgets/apbbar_login.dart';
import 'widgets/avatar_login.dart';
import 'widgets/button_login.dart';
import 'widgets/message_text_button.dart';
import 'widgets/login_text_field.dart';
import 'widgets/text_name_login.dart';

class LoginScreen extends StatefulWidget {
  final String? userLogin;

  final String? paswwUser;

  LoginScreen({
    @required this.userLogin,
    @required this.paswwUser,
  });

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController passwController = TextEditingController();
  TextEditingController userController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              AppBarLogin(title: 'MyFirstBank'),
              AvatarLogin(avatarTitle: 'AT'),
              LoginTextName(name: 'Arles Tabares'),
              LoginTextField(
              
                label: 'Identificación',
                controller: userController,
                obscureText: false,
              ),
              LoginTextField(
                label: 'password',
                controller: passwController,
                icon: iconTextFieldLogin,
                obscureText: true,
              ),
              MessageTextButton(message: '¿Haz olvidado tu clave de acceso?'),
              LoginButton(
                usuario: this.widget.userLogin,
                passwd: this.widget.paswwUser,
                pressed: () {
                  setState(() {});
                  if (passwController.text == passLogin &&
                      userController.text == userLogin) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyFirstBankHomePage(),
                      ),
                    );
                  } else {
                    return null;
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
