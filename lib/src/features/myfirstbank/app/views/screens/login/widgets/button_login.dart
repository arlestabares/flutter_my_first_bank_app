import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final String? usuario;
  final String? passwd;
  final VoidCallback? pressed;

  const LoginButton({
    @required this.usuario,
    @required this.passwd,
    @required this.pressed,
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.symmetric(
            horizontal: 45.0,
            vertical: 15.0,
          ),
          //primary: kBackgroundColor.withOpacity(0.99),
          primary:  Color(0xFF042A4E),
          onPrimary: Colors.white,
        ),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          //width: 80.0,
          child: Text(
            'Continuar',
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
            ),
          ),
        ),
        onPressed: pressed,
      ),
    );
  }
}
//  Container(
//               padding: EdgeInsets.symmetric(horizontal: 45.0),
//               child: ElevatedButton(

// child: Text(
//   'Continuar',
//   style: TextStyle(
//     color: Colors.white,
//     fontSize: 17,
//   ),
// ),
//                 style: ElevatedButton.styleFrom(
//                   padding: EdgeInsets.symmetric(
//                     horizontal: 45.0,
//                     vertical: 15.0,
//                   ),
//                   primary: kBackgroundColor,
//                   onPrimary: Colors.white,
//                 ),
//               ),
//             )
