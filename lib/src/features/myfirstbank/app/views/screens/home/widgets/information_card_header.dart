import 'package:flutter/material.dart';

class InformationCardHeader extends StatelessWidget {
  final String? description;
  final VoidCallback? pressed;

  const InformationCardHeader({
    @required this.description,
    @required this.pressed,
  });
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: (size.width <= 450) ? size.width * 0.4 : size.width * 0.35,
      padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 8.0),
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 8.0),
      decoration: BoxDecoration(
        color: Color(0xFF042A4E),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            onPressed: pressed,
            child: Text(
              description!,
              style: TextStyle(
                color: Colors.blue[400],
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
