import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/detallesDeCuenta/account_details_screen.dart';

import '../../gastos/money_expenses.dart';
import '../../transferencias/transfer_menu.dart';
import '../widgets/information_card_header.dart';

class MyListViewHorizontalHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.15,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: [
          InformationCardHeader(
            pressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
                'accountDetailsScreen', (route) => false),
            // Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //     builder: (_) => AccountDetailsScreen(),
            //   ),
            // ),
            description: 'Ver patrimonio',
          ),
          InformationCardHeader(
              pressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (_) => MoneyExpensesScreen())),
              description: 'Ver gastos'),
          InformationCardHeader(
              pressed: () => Navigator.push(context,
                  MaterialPageRoute(builder: (_) => AccountDetailsScreen())),
              description: 'Ver ingresos'),
          InformationCardHeader(
              pressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => TransfersInformationScreen())),
              description: 'Transferencias'),
        ],
      ),
    );
  }
}
