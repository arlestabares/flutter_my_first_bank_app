import 'package:flutter/material.dart';

class AcoountsCard extends StatelessWidget {
  final int? accountNumber;
  final int? amount;
  final VoidCallback? pressed;
  final String? titleAccount;

  AcoountsCard({
    required this.accountNumber,
    @required this.amount,
    @required this.pressed,
    @required this.titleAccount,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(top: 10.0),
      margin: EdgeInsets.only(top: 5.0),
      height: (size.height <= 450) ? size.height * 0.20 : size.height * 0.17,
      // width: size.width - 30.0,
      decoration: BoxDecoration(
        //border: Border.all(color: Colors.white),
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              offset: Offset(0, 25),
              blurRadius: 25.0,
              spreadRadius: -30),
        ],
      ),
      child: Card(
        shape: RoundedRectangleBorder(
            //borderRadius: BorderRadius.circular(15.0),

            ),
        child: ListTile(
          onTap: pressed,
          title: Text(titleAccount!),
          subtitle: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 30),
                    Text(
                      'cuenta*$accountNumber',
                      style: TextStyle(color: Colors.blue),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      '*$accountNumber',
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.normal),
                    )
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    '\$ $amount',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w800),
                  ),
                  Text(
                    'Disponible',
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
