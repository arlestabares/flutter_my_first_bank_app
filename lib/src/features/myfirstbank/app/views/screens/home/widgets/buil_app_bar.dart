import 'package:flutter/material.dart';

import '../../../../utils/constants.dart';


AppBar buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: kBackgroundColor,
    elevation: 0.0,
    leading: IconButton(
      onPressed: () => Navigator.pop(context),
      icon: Icon(Icons.arrow_back),
    ),
  );
}