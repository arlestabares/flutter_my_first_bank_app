import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/detallesDeCuenta/account_details_screen.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/home/widgets/accounts_card.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/home/horizontalheader/list_view_horizontal_header.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/home/widgets/my_first_bank_header.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/model_list/amount_account.dart';

class MyFirstBankBodyScreen extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Stack(
        children: [
          HeaderMyFirstBankHomeScreen(
            
          ),
          SafeArea(
            child: Column(
              children: [
              //
                MyListViewHorizontalHeader(),

                //
                AcoountsCard(
                    titleAccount: 'Cuenta',
                    accountNumber: 1234,
                    amount: saldoA-gastos,
                    pressed: () => Navigator.push(context,
                        MaterialPageRoute(builder: (_) => AccountDetailsScreen()))),

                AcoountsCard(
                  titleAccount: 'Cuenta',
                  accountNumber: 1234,
                  amount: saldoA-gastos,
                  pressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (_) => AccountDetailsScreen())),
                ),

                AhorroEInversionCard(),
                AhorroEInversionCard(),
                SizedBox(height: 40.0),
                Text(''),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class AhorroEInversionCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: Card(
        child: Container(
          width:size.width * 0.9 ,
          height: 180.0,
          color: Colors.white,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Ahorro e Inversion',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 21,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20.0),
                Text(
                  'MyFirstBank',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
