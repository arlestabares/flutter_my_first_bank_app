import 'package:flutter/material.dart';

import '../../../../utils/estilo_boxshadow.dart';
import '../../../../widgets/shared.dart';
import '../account_information_more_screen.dart';

class AccountBodyCard extends StatelessWidget {
  final int? accountNumber;
  final int? amountAvailable;
  final int? currentAmount;
  final String? currentAmountText;
  final String? available;
  final String? accountName;

  AccountBodyCard({
    @required this.accountNumber,
    @required this.amountAvailable,
    @required this.available,
    @required this.accountName,
    @required this.currentAmount,
    @required this.currentAmountText,
  });
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(10.0),
      width: double.infinity,
      height: (size.width <= 450) ? size.height * 0.30 : size.height * 0.20,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.white.withOpacity(0.1)),
        boxShadow: [
          kBoxShadow,
        ],
      ),
      child: Column(
        children: [
          buildTextTitleVariaton1(accountName!),
          SizedBox(height: 10.0),
          Text('.$accountNumber'),
          SizedBox(height: 10.0),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //Text('$amountAvailable'),
                      buildTitle('\$ $amountAvailable'),
                      Text(available!),
                    ],
                  ),
                ),
                Spacer(),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Text('$amountAvailable'),
                      buildTitle('$currentAmount'),
                      Text(currentAmountText!),
                    ],
                  ),
                ),
              ],
            ),
          ),
          TextButton(
            child: Text('Más Información'),
            onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => AccountInformationMoreScreen(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
