import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/utils/constants.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/detallesDeCuenta/widgets/alias_card.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/detallesDeCuenta/widgets/list_title_item.dart';

class AccountInformationMoreScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Más Informacion'),
        backgroundColor: kBackgroundColor,
      ),
      body: InformationList(
        numberAccount: 1234,
      ),
    );
  }
}

class InformationList extends StatelessWidget {
  final int? numberAccount;

  InformationList({
    @required this.numberAccount,
  });

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            AliasCard(
              alias: 'Alias',
              accountNumber: 1234,
              ibanNumber: 'ES56 0182 3107 110',
              iconEdit: TextButton(onPressed: () {}, child: Text('Editar')),
              bookEdit: TextButton(onPressed: () {}, child: Text('Editar')),
            ),
            ListTitleItem(
              leading: CircleAvatar(
                child: Icon(Icons.people),
              ),
              title: 'INTERVINIENTES\n',
              subTitle: 'Arles de Jesus Tabares',
              trailing: Text('Titular', style: TextStyle(color: Colors.blue)),
            ),
            ListTitleItem(
              leading: CircleAvatar(
                child: Icon(Icons.home),
              ),
              title: 'DOMICILIO DE CORRESPONDENCIA\n',
              subTitle: 'Montenegro Quindio',
              trailing: TextButton(
                onPressed: () {},
                child: Text((size.width <= 450)
                    ? 'Cambiar \nDomicilio'
                    : 'Cambiar Domicilio'),
              ),
            ),
            ListTitleItem(
              leading: CircleAvatar(
                child: Icon(Icons.people),
              ),
              title: 'CONFIGURACION\n',
              subTitle: 'Opciones de producto',
              trailing: TextButton(
                onPressed: () {},
                child: Text('Titular',),
              ),
            ),
            ListTitleItem(
              leading: CircleAvatar(
                child: Icon(Icons.account_circle),
              ),
              title: 'ESTADO DE LA CUENTA\n',
              subTitle: 'Cuenta corriente',
              trailing: TextButton(
                onPressed: () {},
                child: Text('Activa'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
