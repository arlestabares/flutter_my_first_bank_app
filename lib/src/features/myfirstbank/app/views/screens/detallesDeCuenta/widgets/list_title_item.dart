import 'package:flutter/material.dart';

class ListTitleItem extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final Widget? trailing;
  final Widget? leading;
  final Icon? icon;
  final VoidCallback? pressed;

  const ListTitleItem({
    @required this.title,
    @required this.subTitle,
    this.trailing,
    this.pressed,
    this.icon,
    this.leading,
  });
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(top: 15.0),
      height: (size.width <= 400) ? size.height * 0.25 : size.height * 0.15,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 10),
            color: Colors.grey.withOpacity(0.4),
            blurRadius: 10,
          ),
        ],
      ),
      margin: EdgeInsets.only(top: 10.0),
      child: Column(
        children: [
          ListTile(
            leading: leading,
            title: Text(
              title!,
              style: TextStyle(fontSize: 15.0, color: Colors.black),
            ),
            subtitle: Text(
              subTitle!,
              style: TextStyle(color: Colors.black, fontSize: 17),
            ),
            trailing: trailing,
          ),
        ],
      ),
    );
  }
}
