import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/detallesDeCuenta/widgets/account_body_card.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/model_list/amount_account.dart';

import '../../../utils/constants.dart';

class AccountDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: kBackgroundColor,
          elevation: 0.0,
          title: Text('Cuentas'),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  'myFirstBankHomePage', (route) => false);
            },
            icon: Icon(Icons.arrow_back),
          )
          // leading: IconButton(
          ),
      body: ListView(
        // shrinkWrap: true,
        children: [
          AccountBodyCard(
            accountName: 'Cuenta Corriente',
            accountNumber: 1234,
            amountAvailable: saldoA - gastos,
            available: 'Saldo disponible',
            currentAmount: saldoA - gastos,
            currentAmountText: 'Saldo actual',
          ),
          AccountBodyCard(
            accountName: 'Cuenta de ahorros',
            accountNumber: 1234,
            amountAvailable: saldoA - gastos,
            available: 'Saldo disponible',
            currentAmount: saldoA - gastos,
            currentAmountText: 'Saldo actual',
          ),
        ],
      ),
    );
  }
}
