import 'package:flutter/material.dart';

import '../../../../utils/estilos_text_style.dart';

class AliasCard extends StatelessWidget {
  final int? accountNumber;
  final String? ibanNumber;
  final String? alias;
  final VoidCallback? pressed;
  final Widget? iconEdit;
  final Widget? bookEdit;

  const AliasCard({
    @required this.accountNumber,
    @required this.ibanNumber,
    @required this.iconEdit,
    @required this.alias,
    this.pressed,
    this.bookEdit,
  });
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: (size.width <= 400) ? size.height * 0.30 : size.height * 0.15,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 10),
            color: Colors.grey.withOpacity(0.4),
            blurRadius: 10,
          ),
        ],
      ),
      padding: EdgeInsets.all(12.0),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '$alias\nCuenta *$accountNumber',
                style: styleText,
              ),
              SizedBox(height: 10.0),
              Text(
                'IBAN\n$ibanNumber',
                style: styleText,
              ),
            ],
          ),
          Spacer(),
          Column(
            children: [
              iconEdit!,
              bookEdit!,
            ],
          ),
        ],
      ),
    );
  }
}
