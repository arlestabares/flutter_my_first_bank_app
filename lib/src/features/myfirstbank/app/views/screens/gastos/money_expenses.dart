import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/model_list/amount_account.dart';

import '../../../utils/constants.dart';
import 'widgets/expenses_header_background.dart';

class MoneyExpensesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gastos'),
        backgroundColor: kBackgroundColor,
        elevation: 0.0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ExpensesHeaderBackground(),
            CircularIndicatorExpenses(),
          ],
        ),
      ),
    );
  }
}

class CircularIndicatorExpenses extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          height: size.height * 0.60,
          width: double.infinity,
          color: Colors.white,
          child: CustomPaint(
            painter: MyRadialCustonPaint(),
          ),
        ),
        Positioned(
          //top: 250.0,
          top: (size.width< 400)? size.height / 2 - 190 : size.height /2 - 220,
          //left: 180.0,
          left: size.width / 2 - 41,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      '\$ $gastos',
                      style: TextStyle(color: Colors.black, fontSize: 23),
                    ),
                    Text('Gastos', style: TextStyle(color: Colors.black, fontSize: 17)),
                    Text('Total mes')
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class MyRadialCustonPaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2 - 10);
    final radius = 160.0;
    final paint = Paint()
      ..color = Color(0xFF00897B)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 9;
    canvas.drawCircle(center, radius, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
