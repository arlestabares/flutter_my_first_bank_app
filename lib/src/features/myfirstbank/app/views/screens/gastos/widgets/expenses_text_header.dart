import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/model_list/amount_account.dart';


class ExpensesTextHeader extends StatelessWidget {
  final double? balanceTotal;

  ExpensesTextHeader({@required this.balanceTotal});
  @override
  Widget build(BuildContext context) {
    int? balance = saldoA - gastos;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '\$ $balance',
            style: TextStyle(fontSize: 25.0, color: Colors.white),
          ),
          SizedBox(height: 10.0),
          Text(
            'Balance total',
            style: TextStyle(
              color: Colors.white.withOpacity(0.8),
              fontSize: 19,
              fontWeight: FontWeight.w200,
            ),
          ),
        ],
      ),
    );
  }
}
