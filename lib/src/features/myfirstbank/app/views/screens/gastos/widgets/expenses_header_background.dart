import 'package:flutter/material.dart';

import '../../../../utils/constants.dart';
import 'expenses_text_header.dart';

class ExpensesHeaderBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          height: size.height * 0.30,
          color: kBackgroundColor,
          child: ExpensesTextHeader(
            balanceTotal: 5432,
          ),
        ),
      ],
    );
  }
}
