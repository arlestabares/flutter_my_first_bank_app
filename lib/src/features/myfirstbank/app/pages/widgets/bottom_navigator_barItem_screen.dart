import 'package:flutter/material.dart';

class BottomNavigatorBarItemScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(
            Icons.home,
            color: Colors.black,
          ),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.business,
            color: Colors.black,
          ),
          label: 'Business',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.money_rounded,
            color: Colors.black,
          ),
          label: 'Finance',
        ),
      ],
    );
  }
}
