import 'package:flutter/material.dart';

import '../utils/constants.dart';
import '../views/screens/home/my_first_bank_body_screen.dart';
import 'widgets/bottom_navigator_barItem_screen.dart';

class MyFirstBankHomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigatorBarItemScreen(),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: kBackgroundColor,
        automaticallyImplyLeading: false,
        title: Text('Hola Arles Tabares'),
      ),
      body: MyFirstBankBodyScreen(),
    );
  }
}
