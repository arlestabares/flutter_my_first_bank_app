import 'package:flutter/material.dart';

final styleTextNameHomePage = TextStyle(
  color: Colors.white.withOpacity(0.6),
  fontSize: 21,
  fontWeight: FontWeight.bold,
);


final styleTextMessageLogin = TextStyle(
  color: Colors.white,
  fontSize: 21,
  fontWeight: FontWeight.normal,
);

final styleTextNameLogin = TextStyle(
  color: Colors.white38,
  fontSize: 21,
  fontWeight: FontWeight.w300,
);
final styleText = TextStyle(
  color: Colors.white.withOpacity(0.8),
  fontSize: 21,
  fontWeight: FontWeight.w200,
);
