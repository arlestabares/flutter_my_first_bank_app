import 'package:flutter/material.dart';

const kBackgroundColorAvatar = Color(0xFF004466);
const kBackgroundColor = Color(0xff004481);
const kPrimaryColor = Color(0xFF338);
const kTextColor = Color(0xFF222B45);
const kShadowColor = Color(0xFFE6E6E6);
final kShadowColor2 = Color(0xFFB7B7B7).withOpacity(.16);

const kTextLightColor = Color(0xFF959595);
const kTitleTextColor = Color(0xFF303030);

const kHeadingTextStyle = TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.w600,
);

const kSubTextStyle = TextStyle(fontSize: 16, color: kTextLightColor);

const kTitleTextStyle = TextStyle(
  fontSize: 18,
  color: kTitleTextColor,
  fontWeight: FontWeight.bold,
);

const kBackgroundColorButton = Color(0xFF042A4E);
const kBackgroundColorCircularAvatar = Color(0xFF042A4E);
