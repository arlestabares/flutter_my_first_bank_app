import 'package:flutter/material.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/detallesDeCuenta/account_details_screen.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/detallesDeCuenta/account_information_more_screen.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/gastos/money_expenses.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/app/views/screens/transferencias/transfer_menu.dart';

import '../pages/home_page.dart';

final Map<String, Widget Function(BuildContext)> appRoute = {
  'myFirstBankHomePage': (_) => MyFirstBankHomePage(),
  'transfersInformationScreen': (_) => TransfersInformationScreen(),
  'accountInformationMoreScreen': (_) => AccountInformationMoreScreen(),
  'accountDetailsScreen': (_) => AccountDetailsScreen(),
  'moneyExpensesScreen': (_) => MoneyExpensesScreen(),
};
