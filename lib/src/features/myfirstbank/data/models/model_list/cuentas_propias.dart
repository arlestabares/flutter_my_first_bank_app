class CuentasPropias {
  final String alias;
  final String name;
  final String iban;
  final String currentOrSavings;
  final String amount;

  CuentasPropias(
    this.alias,
    this.name,
    this.iban,
    this.currentOrSavings,
    this.amount,
  );
}

List<CuentasPropias> ownAccounts = <CuentasPropias>[
  CuentasPropias(
    'Cuentas propias',
    'Cuentas propias',
    'Cuentas propias',
    'Cuentas propias',
    'Cuentas propias',
  ),
  CuentasPropias(
    'AT',
    'Arles Tabares',
    'ES56 0182 3107',
    'Ahorros',
    'saldo',
  ),
  CuentasPropias(
    'TA',
    'Arles Tabares C',
    'ES45 0182 1102',
    'Corriente',
    'saldo',
  ),
];
