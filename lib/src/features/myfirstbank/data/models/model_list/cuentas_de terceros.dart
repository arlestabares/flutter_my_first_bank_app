class CuentasDeTerceros {
  String? alias;
  String? name;
  String? accountNumber;
  String? currentOrSavings;
  String? userList ;

  CuentasDeTerceros(
    this.alias,
    this.name,
    this.accountNumber,
    this.currentOrSavings,
  );
}
class CuentasDeTercerosProvider{
List<CuentasDeTerceros> userList = <CuentasDeTerceros>[
  CuentasDeTerceros(
    'Buscar beneficiario',
    'Buscar beneficiario',
    'Buscar beneficiario',
    'Buscar beneficiario',
  ),
  CuentasDeTerceros(
    'JQ',
    'Juan Quezada',
    'ES5684636342',
    'Corriente',
  ),
  CuentasDeTerceros(
    'PA',
    'Pedro Antonio',
    'ES5684636340',
    'Ahorros',
  ),
  CuentasDeTerceros(
    'JM',
    'Juliana Moncada',
    'ES5684636349',
    'Corriente',
  ),
  CuentasDeTerceros(
    'JD',
    'Juan David Toro',
    'ES5684636345',
    'Ahorros',
  ),
];
List<String> miLista = [
  'ES5601823107',
  'ES5693847',
  'ES567463',
  'ES653425',
];
}