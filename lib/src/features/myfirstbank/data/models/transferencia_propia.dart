import 'package:floor/floor.dart';

@Entity(tableName: 'person')
class TransferenciaPropiaDataEntity {
  @PrimaryKey(autoGenerate: true)
  final int? id;
  final int monto;
  final String moneda;
  final String cuentaOrigen;
  final String cuentaDestino;
  final String descripcion;
  final int numeroTransaccion;

  TransferenciaPropiaDataEntity(
    this.monto,
    this.moneda,
    this.cuentaOrigen,
    this.cuentaDestino,
    this.descripcion,
    this.numeroTransaccion, {
    this.id,
  });
}
