import 'package:equatable/equatable.dart';
import 'package:floor/floor.dart';

@Entity(tableName: 'agregar_cuenta_propia')
class AgregarCuentaPropiaDataEntity extends Equatable {
  @PrimaryKey(autoGenerate: true)
  final int? id;
  final String alias;
  final String nombre;
  final String numeroCuenta;
  final String nombreCuenta;
  final String descripcion;

  AgregarCuentaPropiaDataEntity(
    this.alias,
    this.nombre,
    this.numeroCuenta,
    this.nombreCuenta,
    this.descripcion, {
    this.id,
  });

  @override
  List<Object?> get props => [];
}
