//
import 'package:equatable/equatable.dart';
import 'package:floor/floor.dart';

//
@Entity(tableName: 'agregar_cuentas_terceros')
class AgregarCuentaDeTerceroDataEntity extends Equatable {
  @PrimaryKey(autoGenerate: true)
  final int? id;
  final String? nombre;
  final String? numeroCuenta;
  final String? nombreCuenta;
  final String? nombreBanco;

  AgregarCuentaDeTerceroDataEntity(
    this.nombre,
    this.numeroCuenta,
    this.nombreCuenta,
    this.nombreBanco, {
    this.id,
  });

  @override
  List<Object?> get props => [];
}
