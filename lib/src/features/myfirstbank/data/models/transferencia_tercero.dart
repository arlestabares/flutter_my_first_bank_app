import 'package:floor/floor.dart';

@Entity(tableName: 'user_transfer')
class TransferenciaATerceroDataEntity {
  @PrimaryKey(autoGenerate: true)
  int? id;
  int? monto;
  int? numeroTransaccion;
  String? cuentaOrigin;
  String? cuentaDestino;
  String? moneda;
  String? description;
  String nombreBanco;

  TransferenciaATerceroDataEntity(
    this.monto,
    this.numeroTransaccion,
    this.cuentaOrigin,
    this.cuentaDestino,
    this.moneda,
    this.description,
    this.nombreBanco, {
    this.id,
  });
}
