import 'package:floor/floor.dart';

import '../../../models/agregar_cuenta_de_tercero.dart';

abstract class AgregarCuentaDeTerceroDao {
//
  @Insert(onConflict: OnConflictStrategy.ignore)
  Future<void> insertAccountToThird(AgregarCuentaDeTerceroDataEntity userTransferModel);

  @Query('DELETE FROM user_transfer')
  Future<void> deleteAllAccountToThirdAsFuture();
  //
  @Query('DELETE FROM user_transfer WHERE id = :id')
  Future<int> deleteAccountToThirdByIdAsFuture(int id);

  @Query('SELECT * FROM user_transfer WHERE iban LIKE  :iban')
  Stream<AgregarCuentaDeTerceroDataEntity> findAccountToThirdByIbanAsStream(
      String iban);

  @Query('SELECT * FROM user_transfer WHERE name LIKE :name')
  Stream<List<AgregarCuentaDeTerceroDataEntity>> findAccountToThirdByNamesLikeAsStream(
      String name);

  @Query('SELECT * FROM  user_transfer')
  Stream<List<AgregarCuentaDeTerceroDataEntity>> findAllAccountToThirdAsStream();

  //
  @Query('SELECT * FROM user_transfer')
  Future<List<AgregarCuentaDeTerceroDataEntity>> getAllAccountToThirdAsFuture();

  @Update(onConflict: OnConflictStrategy.replace)
  Future<AgregarCuentaDeTerceroDataEntity> updateAccountToThird();
}
