import 'package:floor/floor.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_propia.dart';

@dao
abstract class AgregarCuentaPropiaDao {
  @insert
  Future<void> insertAccountOwn(AgregarCuentaPropiaDataEntity user);
  //
  @Query('SELECT * FROM  Person')
  Future<List<AgregarCuentaPropiaDataEntity>> findAllAccountOwnAsFuture();

  @Update(onConflict: OnConflictStrategy.replace)
  Future<AgregarCuentaPropiaDataEntity?> updateAccountOwn(
      AgregarCuentaPropiaDataEntity user);
  //
  @Query('SELECT * FROM Person WHERE id = :id')
  Stream<AgregarCuentaPropiaDataEntity?> findAccountOwnByIbanAsStream(String id);

  @Query('SELECT * FROM Person')
  Stream<List<AgregarCuentaPropiaDataEntity?>> findAllAccountOwnAsStream();
  //

}
