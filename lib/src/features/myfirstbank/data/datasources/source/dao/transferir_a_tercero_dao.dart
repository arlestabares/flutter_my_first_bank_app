import 'package:floor/floor.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_tercero.dart';

@dao
abstract class TransferirATerceroDao {
//

  @Insert(onConflict: OnConflictStrategy.ignore)
  Future<void> insertUserTransfer(
      TransferenciaATerceroDataEntity userTransferModel);

  @Query('DELETE FROM user_transfer')
  Future<void> deleteAllTransferToThirdAsFuture();
  //
  @Query('DELETE FROM user_transfer WHERE id = :id')
  Future<void> deleteUserTransferByIdAsFuture(int id);

  @Query('SELECT * FROM user_transfer WHERE iban LIKE  :iban')
  Stream<TransferenciaATerceroDataEntity> findTransferToThirdByIbanAsStream(
      String iban);

  @Query('SELECT * FROM user_transfer WHERE name LIKE :name')
  Stream<List<TransferenciaATerceroDataEntity>>
      findTransferToThirdByNamesLikeAsStream(String name);

  @Query('SELECT * FROM  user_transfer')
  Stream<List<TransferenciaATerceroDataEntity>> findAllTransferToThirdAsStream();

  //
  @Query('SELECT * FROM user_transfer')
  Future<List<TransferenciaATerceroDataEntity>>
      getAllTransferToThirdAsFuture();
}
