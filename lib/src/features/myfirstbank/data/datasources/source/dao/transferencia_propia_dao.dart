import 'package:floor/floor.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_propia.dart';


@dao
abstract class TransferenciaCuentaPropiaDao   {
//
  @insert
  Future<void> insertTransferAccountOwn(TransferenciaPropiaDataEntity user);
  //
  @Query('SELECT * FROM  Person')
  Future<List<TransferenciaPropiaDataEntity>> findAllTransfersAccountOwnAsFuture();

  @Update(onConflict: OnConflictStrategy.replace)
  Future<TransferenciaPropiaDataEntity?> updateTransfersAccountOwn(TransferenciaPropiaDataEntity user);
  //
  @Query('SELECT * FROM Person WHERE id = :id')
  Stream<TransferenciaPropiaDataEntity?> findTransfersAccountOwnByIbanAsStream(String id);

  @Query('SELECT * FROM Person')
  Stream<List<TransferenciaPropiaDataEntity?>> findAllTransfersAccountOwnAsStream();
  //

}
