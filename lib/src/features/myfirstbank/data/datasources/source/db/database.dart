import 'package:floor/floor.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_de_tercero.dart';


@Database(version: 1, entities: [AgregarCuentaDeTerceroDataEntity,])
abstract class AppDataBase extends FloorDatabase{}