import 'package:flutter_my_first_bank/src/features/myfirstbank/data/datasources/source/dao/agregar_cuenta_de_tercero_dao.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_de_tercero.dart';

abstract class IAgregarCuentaDeTerceros {
//
  Future<void> insertAccountToThird(AgregarCuentaDeTerceroDataEntity userTransferModel);
  Future<void> deleteAllAccountToThirdAsFuture();
  Future<void> deleteAccountToThirdByIdAsFuture(int id);
  Stream<AgregarCuentaDeTerceroDataEntity> findAccountToThirdByIbanAsStream(
      String iban);
  Stream<List<AgregarCuentaDeTerceroDataEntity>> findAccountToThirdByNamesLikeAsStream(
      String name);
  Stream<List<AgregarCuentaDeTerceroDataEntity>> findAllAccountToThirdAsStream();
  Future<List<AgregarCuentaDeTerceroDataEntity>> getAllAccountToThirdAsFuture();
  Future<AgregarCuentaDeTerceroDataEntity> updateAccountToThird();
}

class AgregarCuentaDeTercerosImpl implements IAgregarCuentaDeTerceros {
//
  final AgregarCuentaDeTerceroDao agregarCuentaDeTerceros;

  AgregarCuentaDeTercerosImpl(this.agregarCuentaDeTerceros);

  @override
  Future<int> deleteAccountToThirdByIdAsFuture(int id) {
    return agregarCuentaDeTerceros.deleteAccountToThirdByIdAsFuture(id);
  }

  @override
  Future<void> deleteAllAccountToThirdAsFuture() {
    return agregarCuentaDeTerceros.deleteAllAccountToThirdAsFuture();
  }

  @override
  Stream<AgregarCuentaDeTerceroDataEntity> findAccountToThirdByIbanAsStream(
      String iban) {
    return agregarCuentaDeTerceros.findAccountToThirdByIbanAsStream(iban);
  }

  @override
  Stream<List<AgregarCuentaDeTerceroDataEntity>> findAccountToThirdByNamesLikeAsStream(
      String name) {
    return agregarCuentaDeTerceros.findAccountToThirdByNamesLikeAsStream(name);
  }

  @override
  Stream<List<AgregarCuentaDeTerceroDataEntity>> findAllAccountToThirdAsStream() {
    return agregarCuentaDeTerceros.findAllAccountToThirdAsStream();
  }

  @override
  Future<List<AgregarCuentaDeTerceroDataEntity>> getAllAccountToThirdAsFuture() {
    return agregarCuentaDeTerceros.getAllAccountToThirdAsFuture();
  }

  @override
  Future<void> insertAccountToThird(
      AgregarCuentaDeTerceroDataEntity userTransferModel) {
    return agregarCuentaDeTerceros.insertAccountToThird(userTransferModel);
  }

  @override
  Future<AgregarCuentaDeTerceroDataEntity> updateAccountToThird() {
    return agregarCuentaDeTerceros.updateAccountToThird();
  }
}
