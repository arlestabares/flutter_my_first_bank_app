import 'package:flutter_my_first_bank/src/features/myfirstbank/data/datasources/source/dao/transferir_a_tercero_dao.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_tercero.dart';

abstract class ITransferirATerceroLocalDataSource {
  Stream<List<TransferenciaATerceroDataEntity>> getAllUserTransferens();
  Stream<TransferenciaATerceroDataEntity> getUserTransferensByIban(
      String iban);
  Stream<List<TransferenciaATerceroDataEntity>> getUserTransferensByName(
      String name);
  Stream<List<TransferenciaATerceroDataEntity>> getAllUserTransferensByName(
      String name);
  Future<void> deleteAllUserTransfer();
  Future<void> deleUserTransferById(int id);

  //

}

//Solo realiza procesos de base de datos
class TransferirATerceroLocalDataSourceImpl
    implements ITransferirATerceroLocalDataSource {
  final TransferirATerceroDao userTransferDao;

  TransferirATerceroLocalDataSourceImpl(this.userTransferDao);

  @override
  Future<void> deleUserTransferById(int id) {
    return userTransferDao.deleteUserTransferByIdAsFuture(id);
  }

  @override
  Future<void> deleteAllUserTransfer() {
    return userTransferDao.deleteAllTransferToThirdAsFuture();
  }

  @override
  Stream<List<TransferenciaATerceroDataEntity>> getAllUserTransferens() {
    return userTransferDao.findAllTransferToThirdAsStream();
  }

  @override
  Stream<List<TransferenciaATerceroDataEntity>> getAllUserTransferensByName(
      String name) {
    return userTransferDao.findTransferToThirdByNamesLikeAsStream(name);
  }

  @override
  Stream<TransferenciaATerceroDataEntity> getUserTransferensByIban(
      String iban) {
    return userTransferDao.findTransferToThirdByIbanAsStream(iban);
  }

  @override
  Stream<List<TransferenciaATerceroDataEntity>> getUserTransferensByName(
      String name) {
    return userTransferDao.findTransferToThirdByNamesLikeAsStream(name);
  }
}
