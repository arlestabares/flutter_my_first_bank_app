import '../../models/transferencia_propia.dart';
import '../source/dao/transferencia_propia_dao.dart';

abstract class ITransferenciaPropia {
  Future<void> insertUserAccount(TransferenciaPropiaDataEntity user);
  Future<List<TransferenciaPropiaDataEntity?>> findAllUsersAccountAsFuture();
  Stream<TransferenciaPropiaDataEntity?> findUserAccountByIbanAsStream(
      String id);
  Stream<List<TransferenciaPropiaDataEntity?>> findAllUserAccountAsStream();
  Future<void> updateUserAccount(TransferenciaPropiaDataEntity user);
}

class TransferenciaPropiaImpl implements ITransferenciaPropia {
  final TransferenciaCuentaPropiaDao userAccountDao;

  TransferenciaPropiaImpl(this.userAccountDao);
  @override
  Stream<List<TransferenciaPropiaDataEntity?>> findAllUserAccountAsStream() {
    return userAccountDao.findAllTransfersAccountOwnAsStream();
  }

  @override
  Future<List<TransferenciaPropiaDataEntity?>>
      findAllUsersAccountAsFuture() {
    return userAccountDao.findAllTransfersAccountOwnAsFuture();
  }

  @override
  Stream<TransferenciaPropiaDataEntity?> findUserAccountByIbanAsStream(
      String id) {
    return userAccountDao.findTransfersAccountOwnByIbanAsStream(id);
  }

  @override
  Future<void> insertUserAccount(TransferenciaPropiaDataEntity user) {
    throw UnimplementedError();
  }

  @override
  Future<void> updateUserAccount(TransferenciaPropiaDataEntity user) {
    return userAccountDao.updateTransfersAccountOwn(user);
  }
}
