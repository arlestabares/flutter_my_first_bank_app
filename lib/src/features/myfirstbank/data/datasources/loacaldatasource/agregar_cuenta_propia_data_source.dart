import 'package:flutter_my_first_bank/src/features/myfirstbank/data/datasources/source/dao/agregar_cuenta_propia_dao.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_propia.dart';

abstract class IAgregarCuentaPropia {
//
  Future<void> insertAccountOwn(AgregarCuentaPropiaDataEntity user);

  Future<List<AgregarCuentaPropiaDataEntity>> findAllAccountOwnAsFuture();

  Future<AgregarCuentaPropiaDataEntity?> updateAccountOwn(AgregarCuentaPropiaDataEntity user);

  Stream<AgregarCuentaPropiaDataEntity?> findAccountOwnByIbanAsStream(String id);

  Stream<List<AgregarCuentaPropiaDataEntity?>> findAllAccountOwnAsStream();
}

//
class AgregarCuentaPropiaImpl implements IAgregarCuentaPropia {
//
  final AgregarCuentaPropiaDao _agregarCuentaPropiaDao;

  AgregarCuentaPropiaImpl(this._agregarCuentaPropiaDao);

  //
  @override
  Stream<AgregarCuentaPropiaDataEntity?> findAccountOwnByIbanAsStream(String id) {
    return _agregarCuentaPropiaDao.findAccountOwnByIbanAsStream(id);
  }

  @override
  Future<List<AgregarCuentaPropiaDataEntity>> findAllAccountOwnAsFuture() {
    return _agregarCuentaPropiaDao.findAllAccountOwnAsFuture();
  }

  @override
  Stream<List<AgregarCuentaPropiaDataEntity?>> findAllAccountOwnAsStream() {
    return _agregarCuentaPropiaDao.findAllAccountOwnAsStream();
  }

  @override
  Future<void> insertAccountOwn(AgregarCuentaPropiaDataEntity user) {
    return _agregarCuentaPropiaDao.insertAccountOwn(user);
  }

  @override
  Future<AgregarCuentaPropiaDataEntity?> updateAccountOwn(AgregarCuentaPropiaDataEntity user) {
    return _agregarCuentaPropiaDao.updateAccountOwn(user);
  }
}
