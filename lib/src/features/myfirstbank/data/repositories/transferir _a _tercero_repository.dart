import 'package:flutter_my_first_bank/src/core/errors/failure.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/datasources/loacaldatasource/transferir_a_tercero_data_source.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_tercero.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/domain/entities/transferencia_a_tercero_entity.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/domain/repositories/transferir_a_tercero_repository.dart';

class TransferirATerceroRepository
    implements ITransferirATerceroRepositoryDomain {
  final ITransferirATerceroLocalDataSource _iTransferirATerceroLocalDataSource;

  TransferirATerceroRepository(this._iTransferirATerceroLocalDataSource);

//
  @override
  Future<void> deleUserTransferById(int id) {
    return _iTransferirATerceroLocalDataSource.deleUserTransferById(id);
  }

  @override
  Future<void> deleteAllUserTransfer() {
    return _iTransferirATerceroLocalDataSource.deleteAllUserTransfer();
  }

  @override
  Stream<List<TransferenciaATerceroDataEntity>> getAllUserTransferens() {
  
  return _iTransferirATerceroLocalDataSource.getAllUserTransferens();
  }

  @override
  Stream<List<TransferenciaATerceroDomainEntity>> getAllUserTransferensByName(
      String name) {
    // TODO: implement getAllUserTransferensByName
    throw UnimplementedError();
  }

  @override
  Stream<TransferenciaATerceroDomainEntity> getUserTransferensByIban(int id) {
    // TODO: implement getUserTransferensByIban
    throw UnimplementedError();
  }

  @override
  Stream<TransferenciaATerceroDomainEntity> getUserTransferensByName(
      String name) {
    // TODO: implement getUserTransferensByName
    throw UnimplementedError();
  }

  @override
  Future<Either<IFailure, TransferenciaATerceroDomainEntity>>
      insertUserTransfer(TransferenciaATerceroDomainEntity userTransferModel) {
    // TODO: implement insertUserTransfer
    throw UnimplementedError();
  }
}
