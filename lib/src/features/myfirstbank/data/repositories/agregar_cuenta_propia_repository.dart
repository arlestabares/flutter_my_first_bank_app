import 'package:flutter_my_first_bank/src/features/myfirstbank/domain/entities/agregar_cuenta_propia_entity.dart';

import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_tercero.dart';

import 'package:flutter_my_first_bank/src/core/errors/failure.dart';

import 'package:dartz/dartz.dart';

import '../../domain/repositories/agregar_cuenta_propia_repository.dart';
import '../models/transferencia_propia.dart';

class AgregarCuentaPropiaRepository
    implements IAgregarCuentaPropiaRepositoryDomain {
  ///
  final IAgregarCuentaPropiaRepositoryDomain
      _iAgregarCuentaPropiaRepositoryDomain;

  AgregarCuentaPropiaRepository(this._iAgregarCuentaPropiaRepositoryDomain);

  @override
  Stream<List<TransferenciaPropiaDataEntity?>> findAllUserAccountAsStream() {
    return _iAgregarCuentaPropiaRepositoryDomain.findAllUserAccountAsStream();
  }

  @override
  Future<List<TransferenciaPropiaDataEntity?>> findAllUsersAccountAsFuture() {
    return _iAgregarCuentaPropiaRepositoryDomain.findAllUsersAccountAsFuture();
  }

  @override
  Stream<TransferenciaPropiaDataEntity?> findUserAccountByIbanAsStream(
      String id) {
    return _iAgregarCuentaPropiaRepositoryDomain
        .findUserAccountByIbanAsStream(id);
  }

  @override
  Future<void> insertAccountOwn(TransferenciaPropiaDataEntity user) {
    return _iAgregarCuentaPropiaRepositoryDomain.insertAccountOwn(user);
  }

  @override
  Future<Either<IFailure, TransferenciaATerceroDataEntity>> updateUserAccount(
      AgregarCuentaPropiaDomainEntity user) {
    return _iAgregarCuentaPropiaRepositoryDomain.updateUserAccount(user);
  }
}
