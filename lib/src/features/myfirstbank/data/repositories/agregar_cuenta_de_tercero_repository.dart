import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_de_tercero.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/domain/repositories/agregar_cuenta_de_tercero_repository.dart';

class AgregarCuentaDeTerceroRepository
    implements IAgregarCuentaDeTerceroRepositoryDomain {
  ///
  final IAgregarCuentaDeTerceroRepositoryDomain
      _iAgregarCuentaDeTerceroRepositoryDomain;

  AgregarCuentaDeTerceroRepository(
      this._iAgregarCuentaDeTerceroRepositoryDomain);

  @override
  Future<void> insertAccountToThird(
      AgregarCuentaDeTerceroDataEntity userTransferModel) {
    return _iAgregarCuentaDeTerceroRepositoryDomain
        .insertAccountToThird(userTransferModel);
  }

  @override
  Future<int> deleteAccountToThirdByIdAsFuture(int id) {
    return _iAgregarCuentaDeTerceroRepositoryDomain
        .deleteAccountToThirdByIdAsFuture(id);
  }

  @override
  Future<void> deleteAllAccountToThirdAsFuture() {
    return _iAgregarCuentaDeTerceroRepositoryDomain
        .deleteAllAccountToThirdAsFuture();
  }

  @override
  Stream<AgregarCuentaDeTerceroDataEntity> findAccountToThirdByIbanAsStream(
      String iban) {
    return _iAgregarCuentaDeTerceroRepositoryDomain
        .findAccountToThirdByIbanAsStream(iban);
  }

  @override
  Stream<List<AgregarCuentaDeTerceroDataEntity>>
      findAccountToThirdByNamesLikeAsStream(String name) {
    return _iAgregarCuentaDeTerceroRepositoryDomain
        .findAccountToThirdByNamesLikeAsStream(name);
  }

  @override
  Stream<List<AgregarCuentaDeTerceroDataEntity>>
      findAllAccountToThirdAsStream() {
    return _iAgregarCuentaDeTerceroRepositoryDomain
        .findAllAccountToThirdAsStream();
  }

  @override
  Future<List<AgregarCuentaDeTerceroDataEntity>>
      getAllAccountToThirdAsFuture() {
    return _iAgregarCuentaDeTerceroRepositoryDomain
        .getAllAccountToThirdAsFuture();
  }

  @override
  Future<AgregarCuentaDeTerceroDataEntity> updateAccountToThird() {
    return _iAgregarCuentaDeTerceroRepositoryDomain.updateAccountToThird();
  }
}
