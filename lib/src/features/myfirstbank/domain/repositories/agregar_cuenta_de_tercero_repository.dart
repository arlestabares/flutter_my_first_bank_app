import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_de_tercero.dart';

abstract class IAgregarCuentaDeTerceroRepositoryDomain {
  Future<void> insertAccountToThird(
      AgregarCuentaDeTerceroDataEntity userTransferModel);
      
  Future<void> deleteAllAccountToThirdAsFuture();
  
   Future<int> deleteAccountToThirdByIdAsFuture(int id);
   
   
   Stream<AgregarCuentaDeTerceroDataEntity> findAccountToThirdByIbanAsStream(
      String iban);
      
      
      Stream<List<AgregarCuentaDeTerceroDataEntity>> findAccountToThirdByNamesLikeAsStream(
      String name);
      
      
      Stream<List<AgregarCuentaDeTerceroDataEntity>> findAllAccountToThirdAsStream();
      
      
      Future<List<AgregarCuentaDeTerceroDataEntity>> getAllAccountToThirdAsFuture();
      
      
      Future<AgregarCuentaDeTerceroDataEntity> updateAccountToThird();
}
