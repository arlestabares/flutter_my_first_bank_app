//Definimos metodos de comunicacion para obtener la data cuando sea implementado.
import 'package:dartz/dartz.dart';
import 'package:flutter_my_first_bank/src/core/errors/failure.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_tercero.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/domain/entities/transferencia_a_tercero_entity.dart';

abstract class ITransferirATerceroRepositoryDomain {
//
  //future que sera implementado por el caso de uso.
  Future<Either<IFailure, TransferenciaATerceroDataEntity>>
      insertUserTransfer(TransferenciaATerceroDomainEntity userTransferModel);

  //
  Stream<List<TransferenciaATerceroDataEntity>> getAllUserTransferens();

  Stream<TransferenciaATerceroDataEntity> getUserTransferensByIban(int id);

  Stream<TransferenciaATerceroDataEntity> getUserTransferensByName(
      String name);

  Stream<List<TransferenciaATerceroDataEntity>> getAllUserTransferensByName(
      String name);

  Future<void> deleteAllUserTransfer();

  Future<void> deleUserTransferById(int id);
}
