import 'package:dartz/dartz.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_propia.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_tercero.dart';

import '../../../../core/errors/failure.dart';
import '../../data/models/transferencia_propia.dart';
import '../entities/agregar_cuenta_propia_entity.dart';

abstract class IAgregarCuentaPropiaRepositoryDomain {
//

 Future<void> insertAccountOwn(TransferenciaPropiaDataEntity user);
  Future<Either<IFailure, TransferenciaATerceroDataEntity>> updateUserAccount(
      AgregarCuentaPropiaDomainEntity user);

  Future<List<TransferenciaPropiaDataEntity?>> findAllUsersAccountAsFuture();

  Stream<List<TransferenciaPropiaDataEntity?>> findAllUserAccountAsStream();

  Stream<TransferenciaPropiaDataEntity?> findUserAccountByIbanAsStream(
      String id);
}
