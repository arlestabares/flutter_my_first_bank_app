import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/agregar_cuenta_de_tercero.dart';

class AgregarCuentaDeTerceroDomainEntity
    extends AgregarCuentaDeTerceroDataEntity {
  //
  AgregarCuentaDeTerceroDomainEntity(String? nombre, String? numeroCuenta,
      String? nombreCuenta, String? nombreBanco)
      : super(
          nombre,
          numeroCuenta,
          nombreCuenta,
          nombreBanco,
        );
}
