//Entity
import 'package:flutter_my_first_bank/src/features/myfirstbank/data/models/transferencia_tercero.dart';

// ignore: must_be_immutable
class TransferenciaATerceroDomainEntity
    extends TransferenciaATerceroDataEntity {
  TransferenciaATerceroDomainEntity(
    int? monto,
    int? numeroTransaccion,
    String? cuentaOrigin,
    String? cuentaDestino,
    String? moneda,
    String? description,
    String nombreBanco,
  ) : super(
          monto,
          numeroTransaccion,
          cuentaOrigin,
          cuentaDestino,
          moneda,
          description,
          nombreBanco,
        );
}
