import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/errors/failure.dart';
import '../../../../core/usecases/global_use_case.dart';
import '../entities/agregar_cuenta_propia_entity.dart';
import '../repositories/agregar_cuenta_propia_repository.dart';

class ActualizarIbanCuentaPropiaUseCase
    implements IGlobalUseCases<AgregarCuentaPropiaDomainEntity, Params> {
  final IAgregarCuentaPropiaRepositoryDomain iCuentasPropiasRepositoryDomain;

  ActualizarIbanCuentaPropiaUseCase(this.iCuentasPropiasRepositoryDomain);

  @override
  Future<Either<IFailure, AgregarCuentaPropiaDomainEntity>> call(
      Params params) {
    throw UnimplementedError();
  }

  //

}

class Params extends Equatable {
// final String modificarIban;
  final AgregarCuentaPropiaDomainEntity userAccountDomainEntity;

  Params(this.userAccountDomainEntity);

  @override
  List<Object?> get props => [];
}
