import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_my_first_bank/src/features/myfirstbank/domain/repositories/transferir_a_tercero_repository.dart';

import '../../../../core/errors/failure.dart';
import '../../../../core/usecases/global_use_case.dart';
import '../entities/transferencia_a_tercero_entity.dart';

class GuardarTransferenciaDeTerceroUseCase
    implements IGlobalUseCases<TransferenciaATerceroDomainEntity, Params> {
  final ITransferirATerceroRepositoryDomain iRepositoryDomain;

  GuardarTransferenciaDeTerceroUseCase(this.iRepositoryDomain);

  @override
  Future<Either<IFailure, TransferenciaATerceroDomainEntity>> call(
      Params params) {
    throw UnimplementedError();
  }
}

class Params extends Equatable {
  final TransferenciaATerceroDomainEntity userTransferDomainEntity;

  Params(this.userTransferDomainEntity);

  @override
  List<Object?> get props => [];
}
