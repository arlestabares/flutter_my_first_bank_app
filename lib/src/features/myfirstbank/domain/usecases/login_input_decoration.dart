import 'package:flutter/material.dart';

final decorationTextField = InputDecoration(
  labelText: 'Contraseña',
  labelStyle: TextStyle(color: Colors.white),
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(color: Colors.white),
  ),
  suffixIcon: Icon(
    Icons.visibility_off_outlined,
    color: Colors.white,
  ),
);

final iconTextFieldLogin = Icon(
  Icons.visibility_off_outlined,
  color: Colors.white,
);



