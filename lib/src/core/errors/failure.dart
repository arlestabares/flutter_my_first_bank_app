import 'package:equatable/equatable.dart';

abstract class IFailure extends Equatable {
  @override
  List<Object?> get props => [];
}

// General Failures
class ServerFailure extends IFailure {}

class CacheFailure extends IFailure {}
