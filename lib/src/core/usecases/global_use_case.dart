import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_my_first_bank/src/core/errors/failure.dart';

abstract class IGlobalUseCases<Type, Params> {
  Future<Either<IFailure, Type>> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object> get props => [];
}
